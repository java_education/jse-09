package ru.t1.oskinea.tm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface FormatUtil {

    static BigDecimal convertBytes(final long bytes, final long base) {
        return BigDecimal.valueOf(bytes).divide(BigDecimal.valueOf(base)).setScale(2, RoundingMode.HALF_EVEN);
    }

    static String formatBytes(final long bytes) {
        final long KILOBYTE = 1024;
        final long MEGABYTE = KILOBYTE * 1024;
        final long GIGABYTE = MEGABYTE * 1024;
        final long TERABYTE = GIGABYTE * 1024;

        if (bytes < KILOBYTE) return bytes + " B";
        else if (bytes < MEGABYTE)
            return convertBytes(bytes, KILOBYTE) + " KB";
        else if (bytes < GIGABYTE)
            return convertBytes(bytes, MEGABYTE) + " MB";
        else if (bytes < TERABYTE)
            return convertBytes(bytes, GIGABYTE) + " GB";
        else
            return convertBytes(bytes, TERABYTE) + " TB";
    }

}
